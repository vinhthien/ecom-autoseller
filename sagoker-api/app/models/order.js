const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const OrderSchema = new mongoose.Schema(
  {
    orderNumber: {
      type: String,
      required: true
    },
    status: {
      type: String,
      required: true
    },
    waybill: {
      type: String,
      required: true
    },
    note: {
      type: String
    },
  },
  {
    versionKey: false,
    timestamps: true
  }
)
OrderSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Order', OrderSchema)
