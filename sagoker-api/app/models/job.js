const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const JobSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String
    },
    deadline: {
      type: Date
    },
    priorty: {
      type: Number,
      default: 0
    },
    assignedBy: {
      type: String
    },
    reminderDate: {
      type: Date
    },
  },
  {
    versionKey: false,
    timestamps: true
  }
)
JobSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Job', JobSchema)
