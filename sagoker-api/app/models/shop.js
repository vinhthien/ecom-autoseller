const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const ShopSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    session: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
ShopSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Shop', ShopSchema)
