import * as React from 'react';
import { StyleSheet, Alert, SectionList } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import YourRestApi from './YourRestApi';
import { Text, View } from '../components/Themed';

export default function TabTwoScreen() {
  
  function loadFirstRows() {
    const api = new YourRestApi();
    api.createCity('thss')
      .then((response: any) => {
        alert(response);
      })   // Successfully logged in
      .then((token: any) => {
        // nothing
      })    // Remember your credentials
      .catch((err: any) => {
        alert(err.message);
      });  // Catch any error
  };
  loadFirstRows();

  return (
    <View style={styles.container}>
      <SectionList
          sections={[
            { title: 'D', data: ['Devin'] },
            { title: 'J', data: ['Jackson', 'James', 'Jillian', 'Jimmy', 'Joel', 'John', 'Julie'] },
          ]}
          renderItem={({ item }) => <Text style={styles.item}>{item}</Text>}
          renderSectionHeader={({ section }) => (
            <Text style={styles.sectionHeader}>{section.title}</Text>
          )}
          keyExtractor={(item, index) => index}
        />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  }
});
