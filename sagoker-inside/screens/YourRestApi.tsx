import RestClient from 'react-native-rest-client';

export default class YourRestApi extends RestClient {
  constructor () {
    // Initialize with your base URL
    super('http://localhost:3000');
  }
  
  login (email: string, password: string) {
    // Returns a Promise with the response.
    return this.POST('/login', { email, password });
  }

  createCity (name: string) {
    // Returns a Promise with the response.
    return this.POST('/city', { name });
  }
  
}