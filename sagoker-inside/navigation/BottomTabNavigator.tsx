import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import TabOneScreen from '../screens/TabOneScreen';
import TabTwoScreen from '../screens/TabTwoScreen';
import TabCustomerScreen from '../screens/TabCustomerScreen';
import TabXuatHangScreen from '../screens/TabXuatHangScreen';
import { BottomTabParamList, TabOneParamList, TabTwoParamList, TabCustomerParamList, TabXuatHangParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabOne"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="Sotalink"
        component={TabOneNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Công việc"
        component={TabTwoNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Khách hàng"
        component={TabCustomerNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Xuất hàng"
        component={TabXuatHangNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="TabOneScreen"
        component={TabOneScreen}
        options={{ headerTitle: 'Sotalink' }}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTwoScreen"
        component={TabTwoScreen}
        options={{ headerTitle: 'Công việc' }}
      />
    </TabTwoStack.Navigator>
  );
}

const TabCustomerStack = createStackNavigator<TabCustomerParamList>();

function TabCustomerNavigator() {
  return (
    <TabCustomerStack.Navigator>
      <TabCustomerStack.Screen
        name="TabCustomerScreen"
        component={TabCustomerScreen}
        options={{ headerTitle: 'Khách Hàng' }}
      />
    </TabCustomerStack.Navigator>
  );
}

const TabXuatHangStack = createStackNavigator<TabXuatHangParamList>();

function TabXuatHangNavigator() {
  return (
    <TabXuatHangStack.Navigator>
      <TabXuatHangStack.Screen
        name="TabXuatHangScreen"
        component={TabXuatHangScreen}
        options={{ headerTitle: 'Xuất Hàng' }}
      />
    </TabXuatHangStack.Navigator>
  );
}