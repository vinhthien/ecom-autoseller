export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
  TabCustomer: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};

export type TabCustomerParamList = {
  TabCustomerScreen: undefined;
};

export type TabXuatHangParamList = {
  TabXuatHangScreen: undefined;
};
