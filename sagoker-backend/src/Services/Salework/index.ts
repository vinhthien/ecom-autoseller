import puppeteer, { Page, Browser, Viewport } from 'puppeteer';

export const getNumberOfInstantOrders = async (): Promise<any | null> => {

  // return {
  //   shopeeInstantOrderNumber: 667,
  //   lazadaInstantOrderNumber: 889
  // };

  let browserInstance;
  try {
    const browserInstance = await newInstance();
    const page = await browserInstance.newPage();
    await loginSalework(page, '0988549089/automation', '123456@Thien');

    const shopeeInstantOrderNumber = await alertShopeeShipping2h(page);
    const lazadaInstantOrderNumber = await alertLazadaShipping2h(page);
    await closeInstance(browserInstance);

    return {
      shopeeInstantOrderNumber: shopeeInstantOrderNumber,
      lazadaInstantOrderNumber: lazadaInstantOrderNumber
    };
  } catch (error) {
    console.log(error);
    if (browserInstance) {
      await closeInstance(browserInstance);
    }
  }
  return null;
};

let chromium: any;

const newInstance = async (): Promise<Browser> => {
  // @TODO: Non-api routes attempt these requires and will fail (chrome-aws-lambda is stripped out)
  // Instead handle the require only when we hit the entrypoint where its actually needed (i.e only api routes)
  // It should be investigated why the require was being handled in the first place. Probably just some tree-shaking required
  if (!chromium) {
    try {
      chromium = require('chrome-aws-lambda');
    } catch (error) {
      throw new Error('chromium failed to load');
    }
  }
  return await puppeteer.launch({
    args: chromium.args,
    defaultViewport: chromium.defaultViewport,
    executablePath: await chromium.executablePath,
    headless: true, // chromium.headless
    ignoreHTTPSErrors: true,
  });
};

const closeInstance = async (browser: Browser): Promise<void> => {
  // close all pages, fix perm issues on windows 10 (https://github.com/puppeteer/puppeteer/issues/298)
  let pages = await browser.pages();
  await Promise.all(pages.map(page =>page.close()));
  return await browser.close();
};

const loginSalework = async (page: Page, username: string, password: string): Promise<void> => {
  await page.goto('https://salework.net/login');
  await page.type('#login', username);
  await page.type('#password', password);
  const navigationPromise = page.waitForNavigation();
  await page.click('[value="Log In"]')
  await navigationPromise;
};

const alertShopeeShipping2h = async (page: Page): Promise<number> => {
  const countSXI = await countFilter(page, "SHOPEE", "Shopee Xpress Instant");
  const countAHA = await countFilter(page, "SHOPEE", "AhaMove");
  return countSXI + countAHA;
};

// Do lazada chưa chuyển giao cho bên vận chuyển, nên filter đơn AHAMOVE sẽ không có kết quả.
const alertLazadaShipping2h = async (page: Page): Promise<number> => {
  let numInstantOrder = 0;
  // Check hoa toc Lazada
  const gotoPromise = await page.goto('https://stock.salework.net/orders');
    await page.click('#__BVID__17__BV_toggle_');
    await page.click('[aria-labelledby="__BVID__17__BV_toggle_"] :nth-child(3) .dropdown-item'); // select Lazada
    
    // Filter hoa toc Lazada (Giả định không có popup nào hiện lên)
    await page.click('#sidebar-sort');
    const selectorPromise = await page.waitForSelector("input[value='prior']");
      await page.click('input[value="prior"]'); // stick vào ô "Đơn hỏa tốc"
      await page.click('button.btn.px-3.btn-primary');
      const timeoutPromise = await page.waitForTimeout(3000);
        let contentInstantOrder = await page.$eval('#nav_state button:nth-child(3) span', el => el.textContent);
        if (contentInstantOrder != null) {
          numInstantOrder = Number(contentInstantOrder.replace("\(", '').replace("\)", '').trim());
        }
      await timeoutPromise;
    await selectorPromise;
  await gotoPromise;
  return numInstantOrder
};

// platform: 'SHOPEE', 'LAZADA'
// logistic: 'Shopee Xpress Instant', 'AhaMove'
const countFilter = async (page: Page, platform: string, logistic: string): Promise<number> => {
  let numInstantOrder = 0;
  const gotoPromise = await page.goto('https://stock.salework.net/orders');
    await page.click('#__BVID__17__BV_toggle_');

    let platformNum = 2; // SHOPEE
    if (platform == "LAZADA") {
      platformNum = 3;
    }
    await page.click('[aria-labelledby="__BVID__17__BV_toggle_"] :nth-child('+platformNum+') .dropdown-item'); // select Platform

    // Filter hoa toc logistics (Giả định không có popup nào hiện lên)
    await page.click('#sidebar-sort');
    const selectorPromise = await page.waitForSelector('#sortLogistic');
      await page.select('#sortLogistic', logistic);
      await page.click('button.btn.px-3.btn-primary');
      const timeoutPromise = await page.waitForTimeout(3000);
        let contentInstantOrder = await page.$eval('#nav_state button:nth-child(3) span', el => el.textContent); // Lấy dữ liệu trong tab "Mới"
        if (contentInstantOrder != null) {
          numInstantOrder = Number(contentInstantOrder.replace("\(", '').replace("\)", '').trim());
        }
        //await page.click('#nav_state button:nth-child(3)'); // Choose "Mới" tab
      await timeoutPromise;
    await selectorPromise;
  await gotoPromise;
  return numInstantOrder;
};

const closeXButton = async (page: Page): Promise<void> => {
  const selectorPromise = await page.waitForSelector('button[class="close"]');
    
  await selectorPromise;
  // const exists = await page.$eval('button[class="close"]', () => true).catch(() => false)
  // if (exists) {
  //   await page.click('button[class="close"]'); // close popup if any
  // }
};