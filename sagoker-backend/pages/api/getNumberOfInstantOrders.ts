import { getNumberOfInstantOrders } from '@/Services/Salework';

export default async (req: any, res: any) => {
  try {

    const result = await getNumberOfInstantOrders();
    res.json(result);
    return;
  } catch (error) {
    res.statusCode = 500;
    res.end();
    return;
  }
};

let chromium: any;

const newInstance = async (): Promise<Browser> => {
  // @TODO: Non-api routes attempt these requires and will fail (chrome-aws-lambda is stripped out)
  // Instead handle the require only when we hit the entrypoint where its actually needed (i.e only api routes)
  // It should be investigated why the require was being handled in the first place. Probably just some tree-shaking required
  if (!chromium) {
    try {
      chromium = require('chrome-aws-lambda');
    } catch (error) {
      throw new Error('chromium failed to load');
    }
  }
  return await puppeteer.launch({
    args: chromium.args,
    defaultViewport: chromium.defaultViewport,
    executablePath: await chromium.executablePath,
    headless: chromium.headless,
    ignoreHTTPSErrors: true,
  });
};

const closeInstance = async (browser: Browser): Promise<void> => {
  return await browser.close();
};
