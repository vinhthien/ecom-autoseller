import React from 'react';

export default function Index() {
  const [showLoading, setShowLoading] = React.useState<boolean>(false);
  const [shopeeInstantOrderNumber, setShopeeInstantOrderNumber] = React.useState<number>(0);
  const [lazadaInstantOrderNumber, setLazadaInstantOrderNumber] = React.useState<number>(0);
  const [readyToSpeak, setReadyToSpeak] = React.useState<boolean>(false);
  const [test, setTest] = React.useState<string>("");
  const audioHoaTocShopeeRef = React.createRef<HTMLAudioElement>();
  const audioHoaTocLazadaRef = React.createRef<HTMLAudioElement>();
  const audioHoaTocShopeeLazadaRef = React.createRef<HTMLAudioElement>();
  const interval = 30 // mins

  // Implementation code where T is the returned data shape
  function api<T>(url: string): Promise<T> {
    return fetch(url)
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        
        return response.json<T>()
      })

  }

  async function speakText(shopeeInstantOrderNum: number, lazadaInstantOrderNum: number) {
    if (shopeeInstantOrderNum > 0 && lazadaInstantOrderNum > 0) {
      if (audioHoaTocShopeeLazadaRef.current) {
        await audioHoaTocShopeeLazadaRef.current.play();
      }
    } else {
      if (shopeeInstantOrderNum > 0) {
        if (audioHoaTocShopeeRef.current) {
          audioHoaTocShopeeRef.current.play();
        }
      }
      if (lazadaInstantOrderNum > 0) {
        if (audioHoaTocLazadaRef.current) {
          audioHoaTocLazadaRef.current.play();
        }
      }
    }
  }

  async function getNumberOfInstantOrders() {
    if (showLoading) {
      return;
    }
    setShowLoading(true);
    
    await api<{ shopeeInstantOrderNumber: number; lazadaInstantOrderNumber: number }>('/api/getNumberOfInstantOrders')
      .then(({ shopeeInstantOrderNumber, lazadaInstantOrderNumber }) => {
        setShopeeInstantOrderNumber(shopeeInstantOrderNumber);
        setLazadaInstantOrderNumber(lazadaInstantOrderNumber);
        setShowLoading(false);
        setReadyToSpeak(true);
      })
      .catch(error => {
        setShowLoading(false);
      })
  }

  setInterval(async function() { // check salework
    await getNumberOfInstantOrders();
  }, interval*60*1000); // 30 mins 

  React.useEffect(() => {
    // code to be run when state variables in
    // dependency array changes
    if (readyToSpeak) {
      setReadyToSpeak(false);
      speakText(shopeeInstantOrderNumber, lazadaInstantOrderNumber);
    }
  }, [readyToSpeak])

  return (
    <div>
      
      <h3>Phần mềm nội bộ SAGOKER</h3>
      <ul>
        <li>
          {test}
          Kiểm tra đơn mỗi {interval} phút
          <br/>
          <button onClick={getNumberOfInstantOrders}>Bắt đầu kiểm tra đơn hỏa tốc.</button>
          {showLoading && "Đang xử lý..."}
          <br />
          <span>Có {shopeeInstantOrderNumber} đơn hỏa tốc SHOPEE</span>
          <br />
          <span>Có {lazadaInstantOrderNumber} đơn hỏa tốc LAZADA</span>
        </li>
      </ul>
      <br />
      <audio ref={audioHoaTocShopeeLazadaRef}>
        <source src='/hoa-toc-shopee-lazada.mp3' type='audio/mpeg' />
        Your browser does not support the audio element.
      </audio>
      <audio ref={audioHoaTocShopeeRef}>
        <source src='/hoa-toc-shopee.mp3' type='audio/mpeg' />
        Your browser does not support the audio element.
      </audio>
      <audio ref={audioHoaTocLazadaRef}>
        <source src='/hoa-toc-lazada.mp3' type='audio/mpeg' />
        Your browser does not support the audio element.
      </audio>
    </div>
  );
}
